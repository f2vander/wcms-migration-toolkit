<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\UwContentParser;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Source plugin for retrieving and formatting content sections.
 *
 * It loads formatted content from the database and splits it into multiple
 * content pieces by using embedded tags as delimiters.
 *
 * @MigrateSource(
 *   id = "uw_content",
 *   source_module = "node"
 * )
 */
class UwContent extends UwTable {

  use UwMigrateTrait;

  /**
   * Custom content parser.
   *
   * @var \Drupal\uw_migrate\UwContentParser
   */
  protected $parser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->parser = new UwContentParser();
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $content_col = $this->configuration['content_column'];
    $fetch_all = !empty($this->configuration['fetch_all']);
    $new_rows = [];

    foreach ($iterator as $row) {
      $content_items = $this->parser->getContentItems($row[$content_col]);

      foreach ($content_items as $delta => $content) {
        // Filter out embedded tags if we don't need all items.
        $ref_id = $this->parser->getReferenceId($content);
        if (!$fetch_all && $ref_id) {
          continue;
        }

        $new_row = $row;
        $new_row[$content_col] = $content;
        $new_row['delta'] = $delta;
        $new_row['ref_id'] = $ref_id;
        $new_rows[] = $new_row;
      }
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

}
