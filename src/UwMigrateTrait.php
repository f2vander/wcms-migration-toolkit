<?php

namespace Drupal\uw_migrate;

trait UwMigrateTrait {

  /**
   * Provides mapping between custom tags and new block types.
   *
   * @todo: Add remaining items.
   */
  public function getTagsMapping() {
    return [
      'ckcalltoaction' => 'uw_cbl_call_to_action',
      'ckcodepen' => '',
      'ckembeddedmaps' => 'uw_cbl_google_maps',
      'ckfacebook' => 'uw_cbl_facebook',
      'ckfactsfigures' => 'uw_cbl_facts_and_figures',
      'ckimagegallery' => 'uw_cbl_image_gallery',
      'ckmailchimp' => '',
      'ckmailman' => 'uw_cbl_mailman',
      'ckpowerbi' => 'uw_cbl_powerbi',
      'cktableau' => 'uw_cbl_tableau',
      'cktimeline' => 'uw_cbl_timeline',
      'cktint' => '',
      'cktwitter' => 'uw_cbl_twitter',
      'ckvimeo' => 'uw_cbl_remote_video',
      'uwvideo' => 'uw_cbl_remote_video',
    ];
  }

  /**
   * Contains mapping between custom tags attributes and source row properties.
   */
  public function getAttributesMapping() {
    return [
      'ckcalltoaction' => 'data-calltoaction-nid',
      'cktimeline' => 'data-timeline-nid',
      'ckfactsfigures' => 'data-factsfigures-nid',
      'ckimagegallery' => 'data-imagegallerynid',
      'uwvideo' => 'href',
      'ckvimeo' => 'data-url',
    ];
  }

  /**
   * Returns the ID attribute value for the given element.
   *
   * ID attribute is taken from self::getAttributesMapping().
   */
  public function getElementId(\DOMElement $element) {
    $attributes = $this->getAttributesMapping();
    if (isset($attributes[$element->nodeName])) {
      return $element->getAttribute($attributes[$element->nodeName]);
    }
    return FALSE;
  }

}
