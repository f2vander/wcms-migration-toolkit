<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Component\Utility\Html;
use Drupal\migrate\Plugin\migrate\source\EmbeddedDataSource;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\UwMigrateTrait;

/**
 * Custom source plugin to split data rows based on available custom tags.
 *
 * @MigrateSource(
 *   id = "uw_embedded_data",
 *   source_module = "uw_migrate"
 * )
 */
class UwEmbeddedDataSource extends EmbeddedDataSource {

  use UwMigrateTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    // Save original rows and clear the list.
    $rows = $this->dataRows;
    $this->dataRows = [];

    // Add a separate row for each embedded tag.
    $uw_tags_mapping = $this->getTagsMapping();
    foreach ($rows as $row) {
      $body = Html::load($row['value']);
      foreach (array_keys($uw_tags_mapping) as $uw_tag) {
        foreach ($body->getElementsByTagName($uw_tag) as $element) {
          $row['tag'] = $uw_tag;
          $this->dataRows[] = $this->extendDataRow($row, $element);
        }
      }
    }
  }

  /**
   * Converts HTML attributes into source row properties.
   * @see:
   */
  protected function extendDataRow($row, \DOMElement $element) {
    $attributes = $this->getAttributesMapping();
    $uw_tag = $element->nodeName;
    $uw_tag_attributes = $attributes[$uw_tag];

    foreach ($element->attributes as $attr) {
      if (isset($uw_tag_attributes[$attr->nodeName])) {
        $row_property = $uw_tag_attributes[$attr->nodeName];
        $row[$row_property] = $attr->nodeValue;
      }
    }

    return $row;
  }

}
