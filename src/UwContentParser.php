<?php

namespace Drupal\uw_migrate;

use Drupal\Component\Utility\Html;
use Symfony\Component\DomCrawler\Crawler;

class UwContentParser {

  use UwMigrateTrait;

  /**
   * Returns a list of content sections for the given HTML string.
   *
   * Content sections will be split by embedded tags.
   *
   * @see \Drupal\uw_migrate\UwContentParser::getTags()
   */
  public function getContentItems($content, $include_delimiters = TRUE) {
    // Clean-up spaces in self-closed tags.
    $content = str_replace(' />', '/>', $content);
    $sections = [];

    // Identify whether content contains any embedded tags. They will be used as
    // delimiters to split the original HTML markup.
    $delimiters = $this->getTags($content);

    while (!empty($delimiters)) {
      $delimiter = array_shift($delimiters);
      $parts = explode($delimiter, $content);

      // Append previous section if there is some meaningful content there.
      if ($this->clean($parts[0])) {
        $sections[] = $parts[0];
      }

      // Append delimiter if requested.
      if ($include_delimiters) {
        $sections[] = $delimiter;
      }

      // Continue with the remaining content.
      if (!empty($parts[1])) {
        $content = $parts[1];
      }
    }

    // This is the original content if delimiters are not available or
    // remaining content after the last content split.
    // We're not performing any normalization in this case.
    if ($this->clean($content)) {
      $sections[] = $content;
    }

    // Normalize content items (mainly to fix HTML markup).
    // We skip delimiters to preserve original self-closed tags.
    foreach ($sections as &$section) {
      if (!$this->getReferenceId($section)) {
        $section = $this->normalize($section);
      }
    }
    return $sections;
  }

  /**
   * Returns a list of specified HTML tags for the given HTML string.
   *
   * By default, a list of embedded tags will be retrieved.
   *
   * @see \Drupal\uw_migrate\UwMigrateTrait::getTagsMapping()
   */
  public function getTags($content, $tags = []) {
    $result = [];
    $crawler = new Crawler($content);
    $tags = empty($tags) ? array_keys($this->getTagsMapping()) : $tags;
    $selector = implode(',', $tags);

    /** @var \DOMElement $element */
    foreach ($crawler->filter($selector) as $element) {
      $result[] = $element->ownerDocument->saveXML($element);
    }

    return $result;
  }

  public function getVideos($content) {
    $result = [];
    $crawler = new Crawler($content);
    $selector = implode(',', ['uwvideo', 'ckvimeo']);

    /** @var \DOMElement $element */
    foreach ($crawler->filter($selector) as $element) {
      if ($url = $this->getElementId($element)) {
        $result[] = $url;
      }
    }

    return $result;
  }

  /**
   * Returns ID of the embedded content for given HTML content.
   *
   * @param $content
   *   String with HTML markup.
   *
   * @return int
   *   ID of the referenced entity found in the html attribute. If ID was not
   * found, then 0 is returned.
   */
  public function getReferenceId($content) {
    $crawler = new Crawler($content);
    $attributes_mapping = $this->getAttributesMapping();
    $selector = implode(',', array_keys($attributes_mapping));

    /** @var \DOMElement $element */
    foreach ($crawler->filter($selector) as $element) {
      $id = $this->getElementId($element);
      return !empty($id) ? $id : 0;
    }

    return 0;
  }

  /**
   * Removes HTML tags and whitespaces from HTML snippet.
   */
  protected function clean($content) {
    return trim(strip_tags($content, '<a><img><iframe>'));
  }

  /**
   * Normalizes an HTML snippet and removes whitespaces.
   */
  protected function normalize($content) {
    return trim(Html::decodeEntities(Html::normalize(trim($content))));
  }

}
