<?php

namespace Drupal\uw_migrate\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\StaticMap;

/**
 * Provides mapping between source and destination text formats.
 *
 * It requires only a source property, mapping will be added automatically.
 * Example:
 *
 * @code
 * process:
 *   body/format:
 *     plugin: uw_formats
 *     source: body/0/format
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "uw_formats"
 * )
 */
class UwFormats extends StaticMap {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration['map'] = [
      'full_html' => 'uw_tf_full_html',
      'plain_text' => 'plain_text',
      'uw_tf_basic' => 'uw_tf_basic',
      'uw_tf_comment' => 'uw_tf_basic',
      'single_page_remote_events' => 'uw_tf_standard',
      'uw_tf_contact' => 'uw_tf_standard',
      'uw_tf_standard' => 'uw_tf_standard',
      'uw_tf_standard_sidebar' => 'uw_tf_standard',
      'uw_tf_standard_site_footer' => 'uw_tf_standard',
      'uw_tf_standard_wide' => 'uw_tf_standard',
    ];
    $this->configuration['default_value'] = 'uw_tf_standard';
  }

}
