<?php

namespace Drupal\uw_migrate\Plugin\migrate\destination;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows migrating content blocks into layout builder components.
 *
 * Requires the following destination properties:
 *   - nid: ID of the host node entity;
 *   - bid: ID of the block content entity. In most cases, it'll be the result
 *     of migration_lookup or entity_lookup migrations;
 *   - section: The delta value (integer) of the layout builder section;
 *   - region: The region name (string) of the layout (e.g. first, content);
 *
 * @MigrateDestination(
 *   id = "layout_builder:block"
 * )
 */
class LayoutBuilderBlock extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('uuid')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, EntityTypeManagerInterface $entity_type_manager, UuidInterface $uuid) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->entityTypeManager = $entity_type_manager;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'id' => [
        'type' => 'integer',
      ],
      'uuid' => [
        'type' => 'string',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->entityTypeManager->getStorage('node')
      ->load($row->getDestinationProperty('nid'));
    if (empty($node)) {
      throw new MigrateSkipRowException('Host node was not found.');
    }
    if (!$node->hasField(OverridesSectionStorage::FIELD_NAME)) {
      throw new MigrateSkipRowException('Host node does not support customized layout.');
    }

    $bid = $row->getDestinationProperty('bid');
    if (empty($bid)) {
      throw new MigrateSkipRowException('Block was not migrated yet.');
    }

    $block_content = $this->entityTypeManager
      ->getStorage('block_content')
      ->load($bid);
    if (empty($block_content)) {
      throw new MigrateSkipRowException('Block content entity was not found.');
    }

    // @todo: If needed, make this configuration array configured in yml.
    $configuration = [
      'id' => 'inline_block:' . $block_content->bundle(),
      'label' => $block_content->label(),
      'provider' => 'layout_builder',
      'label_display' => 0,
      'view_mode' => 'full',
      'block_revision_id' => $block_content->getRevisionId(),
    ];

    // Append a component to given section/region of page layout.
    $sections = $node->get('layout_builder__layout')->getSections();

    // Create a new section if there are no any at the moment.
    if (empty($sections)) {
      $section = new Section($row->getDestinationProperty('default_layout'));
      $node->get('layout_builder__layout')->appendSection($section);
    }
    else {
      $section_delta = $row->getDestinationProperty('section');
      $section = $node->get('layout_builder__layout')->getSection($section_delta);
    }

    $uuid = $this->uuid->generate();
    $region = $row->getDestinationProperty('region');
    $section->appendComponent(new SectionComponent($uuid, $region, $configuration));
    $node->save();

    return [$node->id(), $uuid];
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    $node = $this->entityTypeManager->getStorage('node')->load($destination_identifier['id']);
    // Node is already gone, there is nothing to rollback.
    if (empty($node)) {
      return;
    }

    // Remove component from the layout builder section.
    /** @var \Drupal\layout_builder\Section $section */
    foreach ($node->get('layout_builder__layout')->getSections() as $section) {
      $section->removeComponent($destination_identifier['uuid']);
    }

    $node->save();
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    // There is nothing to describe here.
  }

}
