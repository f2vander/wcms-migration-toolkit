## Commands:

* `drush ms --group=uw` - display available UW migrations.
* `drush mim --group=uw` - run available UW migrations.

## Required configuration

1) Files migration require the following data in settings.php file:
* `uw_migrate_source` represents the location of the source files.
This can be a local file directory containing the source site (e.g. /var/www/docroot),
or the site address (e.q. http://example.com).
* `uw_migrate_site_path` represents the path to the site directory relative to Drupal
  root. Defaults to 'sites/default/files'.

Example:

```
$settings['uw_migrate_source'] = 'https://uwaterloo.ca/web-resources';
$settings['uw_migrate_site_path'] = 'sites/ca.web-resources/files';
```

In this example, the source site URL is https://uwaterloo.ca/web-resources and
the site path is sites/ca.web-resources/files.

## Tips:
  * `skip_on_empty` process plugin doesn't work correctly with multiple values.
  There is a workaround to use `single_value` plugin before the `skip_on_empty`.
  See: https://www.drupal.org/project/drupal/issues/2905929#comment-13537872
  * By default, `changed` property will be overridden when the migration is
    executed with `--update` flag (e.g. `drush mim uw_sidebar --update`).
    `preserve_changed` module was enabled to avoid this behavior.
  *
