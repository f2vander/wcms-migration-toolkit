<?php

namespace Drupal\Tests\uw_migrate\Unit;

use Drupal\uw_migrate\UwContentParser;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Drupal\uw_migrate\UwContentParser
 * @group uw_migrate
 */
class UwMigrateContentParserTest extends TestCase {

  /**
   * @covers ::getContentItems
   * @dataProvider providerGetContentPieces
   */
  public function testGetContentPieces($content, array $expected_content, array $expected_all) {
    $parser = new UwContentParser();
    $this->assertSame($expected_all, $parser->getContentItems($content));
    $this->assertSame($expected_content, $parser->getContentItems($content, FALSE));
  }

  /**
   * Provider for self::testGetContentPieces().
   */
  public function providerGetContentPieces() {
    return [
      [
        '<div>Text</div>',
        ['<div>Text</div>'],
        ['<div>Text</div>'],
      ],
      [
        '<p><ckcalltoaction data-calltoaction-nid="906"/></p>',
        [],
        ['<ckcalltoaction data-calltoaction-nid="906"/>'],
      ],
      [
        '<a href="https://example.com">Test link</a>',
        ['<a href="https://example.com">Test link</a>'],
        ['<a href="https://example.com">Test link</a>'],
      ],
      [
        '<div><p>text</p></div><div><uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/></div>
         <ckcalltoaction data-calltoaction-nid="123"/>
         <p class="catchy-font">Extra text</p>
         <ckcalltoaction data-calltoaction-nid="567"/>
         Another text is here',
        [
          '<div><p>text</p></div><div></div>',
          '<p class="catchy-font">Extra text</p>',
          'Another text is here',
        ],
        [
          '<div><p>text</p></div><div></div>',
          '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
          '<ckcalltoaction data-calltoaction-nid="123"/>',
          '<p class="catchy-font">Extra text</p>',
          '<ckcalltoaction data-calltoaction-nid="567"/>',
          'Another text is here',
        ]
      ]
    ];
  }

  /**
   * @covers ::getTags
   * @dataProvider providerGetTags
   */
  public function testGetTags($content, array $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getTags($content));
  }

  /**
   * Provider for self::testGetContentPieces().
   */
  public function providerGetTags() {
    return [
      [
        '<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>',
        ['<ckcalltoaction data-calltoaction-nid="123"/>'],
      ],
      [
        '<div><p>text</p></div><div></div>
         <ckcalltoaction data-calltoaction-nid="123"/>
         <ckcalltoaction data-calltoaction-nid="567"/>',
        [
          '<ckcalltoaction data-calltoaction-nid="123"/>',
          '<ckcalltoaction data-calltoaction-nid="567"/>',
        ],
      ],
      [
        '<test data-id="906"/>
         <cktimeline data-timeline-id="123" />
         <ckcalltoaction data-calltoaction-nid="123"/>
         <cktwitter>text</cktwitter>
         <ckcalltoaction data-calltoaction-nid="567"/>
         <uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
        [
          '<cktimeline data-timeline-id="123"/>',
          '<ckcalltoaction data-calltoaction-nid="123"/>',
          '<cktwitter>text</cktwitter>',
          '<ckcalltoaction data-calltoaction-nid="567"/>',
          '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
        ],
      ]
    ];
  }

  /**
   * @covers ::getReferenceId
   * @dataProvider providerGetReferenceId
   */
  public function testGetReferenceId($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getReferenceId($content));
  }

  /**
   * Provider for self::testGetReferenceId().
   */
  public function providerGetReferenceId() {
    return [
      ['<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>', '123'],
      ['<div><p>text</p></div><div></div>', 0],
      ['<cktimeline data-timeline-nid="123" />', '123'],
      ['<ckcalltoaction data-timeline-id="123" />', 0],
      ['<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>', 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'],
    ];
  }

  /**
   * @covers ::getVideos
   * @dataProvider providerGetVideos
   */
  public function testGetVideos($content, $expected) {
    $parser = new UwContentParser();
    $this->assertSame($expected, $parser->getVideos($content));
  }

  /**
   * Provider for self::testGetVideos().
   */
  public function providerGetVideos() {
    return [
      [
        '<div><p><ckcalltoaction data-calltoaction-nid="123"/></p></div>',
        [],
      ],
      [
        '<uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/>',
        [
          'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
        ],
      ],
      [
        '<ckvimeo data-url="https://vimeo.com/255086293" />
         <div>some markup</div>
         <div><uwvideo href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"/></div>
         <ckvimeo data-url="https://vimeo.com/259411563"></ckvimeo>',
        [
          'https://vimeo.com/255086293',
          'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
          'https://vimeo.com/259411563',
        ]
      ]
    ];
  }

}
