<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\uw_migrate\UwContentParser;

/**
 * Source plugin for retrieving embedded videos from content value.
 *
 * @MigrateSource(
 *   id = "uw_video",
 *   source_module = "node"
 * )
 */
class UwVideo extends UwTable {

  /**
   * Custom content parser.
   *
   * @var \Drupal\uw_migrate\UwContentParser
   */
  protected $parser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
    $this->parser = new UwContentParser();
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $iterator = parent::initializeIterator();
    $content_col = $this->configuration['content_column'];
    $new_rows = [];

    foreach ($iterator as $row) {
      $videos = $this->parser->getVideos($row[$content_col]);

      foreach ($videos as $href) {
        $new_row = $row;
        $new_row['video_href'] = $href;
        $new_rows[] = $new_row;
      }
    }

    // Return iterator with new rows.
    return new \ArrayIterator($new_rows);
  }

}
