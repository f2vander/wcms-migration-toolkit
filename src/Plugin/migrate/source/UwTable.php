<?php

namespace Drupal\uw_migrate\Plugin\migrate\source;

use Drupal\migrate_plus\Plugin\migrate\source\Table;

/**
 * Extended version of Table source plugin with ability to join tables.
 *
 * Join section is an array and each array item should have the following
 * properties: table, condition, and fields.
 *
 * Example:
 *
 * @code
 * plugin: uw_table
 * table_name: field_data_body
 * fields:
 *   - entity_id
 *   - body_value
 * join:
 *   -
 *   table: node
 *   condition: 't.entity_id = node.nid'
 *   fields:
 *     - uid
 *     - status
 *
 * @MigrateSource(
 *   id = "uw_table"
 * )
 */
class UwTable extends Table {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    if (isset($this->configuration['join'])) {
      foreach ($this->configuration['join'] as $join) {
        $query->join($join['table'], $join['table'], $join['condition']);

        if (!empty($join['fields'])) {
          $query->fields($join['table'], $join['fields']);
        }
      }
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

}
